{
  config,
  lib,
  pkgs,
  ...
}: let
  mod = "Mod4";

  bg = "#282828";
  red = "#cc241d";
  green = "#98971a";
  yellow = "#d79921";
  blue = "#458588";
  purple = "#b16286";
  aqua = "#689d68";
  gray = "#a89984";
  darkgray = "#1d2021";
  lightgray = "#bdae93";
  white = "#000000";
  fonts = {
    names = ["Iosevka NF Medium" "FontAwesome"];
    size = 12.0;
  };
in {
  xsession.windowManager.i3 = {
    enable = true;
    config = {
      modifier = "Mod4";

      fonts = fonts;

      # Use Mouse+$mod to drag floating windows
      floating.modifier = "Mod4";

      bars = [
        {
          fonts = {
            names = ["Iosevka NF Term" "FontAwesome"];
            size = 12.0;
          };
          position = "top";
          statusCommand = "${pkgs.i3status-rust}/bin/i3status-rs ~/.config/i3status-rust/config-main.toml";
          colors = {
            background = bg;
            statusline = lightgray;
            focusedWorkspace = {
              border = lightgray;
              background = lightgray;
              text = bg;
            };
            inactiveWorkspace = {
              border = darkgray;
              background = darkgray;
              text = lightgray;
            };
            activeWorkspace = {
              border = darkgray;
              background = darkgray;
              text = lightgray;
            };
            urgentWorkspace = {
              border = green;
              background = bg;
              text = lightgray;
            };
          };
        }
      ];

      colors = {
        background = lightgray;

        focused = {
          border = lightgray;
          background = lightgray;
          text = bg;
          indicator = purple;
          childBorder = yellow;
        };
        focusedInactive = {
          border = darkgray;
          background = darkgray;
          text = lightgray;
          indicator = purple;
          childBorder = darkgray;
        };
        unfocused = {
          border = darkgray;
          background = darkgray;
          text = lightgray;
          indicator = purple;
          childBorder = darkgray;
        };
        urgent = {
          border = red;
          background = red;
          text = white;
          indicator = red;
          childBorder = red;
        };
      };

      keybindings = {
        # start a terminal
        "${mod}+Return" = "exec alacritty";

        # start rofi
        "${mod}+i" = "exec --no-startup-id ${pkgs.rofi}/bin/rofi -show combi -run-command \"i3-msg exec '{cmd}'\" -show-icons";

        # kill focused window
        "${mod}+Shift+q" = "kill";

        # change focus
        # "${mod}+h" = "focus left";
        # "${mod}+j" = "focus down";
        # "${mod}+k" = "focus up";
        # "${mod}+l" = "focus right";

        # change focus with arrow keys:
        "${mod}+Left" = "focus left";
        "${mod}+Down" = "focus down";
        "${mod}+Up" = "focus up";
        "${mod}+Right" = "focus right";

        # move focused window
        "${mod}+Shift+h" = "move left";
        "${mod}+Shift+j" = "move down";
        "${mod}+Shift+k" = "move up";
        "${mod}+Shift+l" = "move right";

        # move focused window with arrow keys
        "${mod}+Shift+Left" = "move left";
        "${mod}+Shift+Down" = "move down";
        "${mod}+Shift+Up" = "move up";
        "${mod}+Shift+Right" = "move right";

        # split in horizontal orientation
        "${mod}+Shift+v" = "split h";

        # split in vertical orientation
        # "${mod}+v" = "split v";

        # enter fullscreen mode
        "${mod}+f" = "fullscreen toggle";

        # change container layout
        "${mod}+s" = "layout stacking";
        "${mod}+w" = "layout tabbed";
        "${mod}+e" = "layout toggle split";

        # toggle tiling / floating
        "${mod}+Shift+space" = "floating toggle";

        # change focus between tiling / floating windows
        "${mod}+space" = "focus mode_toggle";

        # focus the parent container
        "${mod}+a" = "focus parent";

        # focus the child container
        "${mod}+d" = "focus child";

        # Scratchpad
        "${mod}+Shift+minus" = "move scratchpad";
        "${mod}+minus" = "scratchpad show";

        # switch to workspace
        "${mod}+1" = "workspace 1";
        "${mod}+2" = "workspace 2";
        "${mod}+3" = "workspace 3";
        "${mod}+4" = "workspace 4";
        "${mod}+5" = "workspace 5";
        "${mod}+6" = "workspace 6";

        # Move workspace across monitors
        "${mod}+Control+Up" = "move workspace to output up";
        "${mod}+Control+Down" = "move workspace to output down";
        "${mod}+Control+Left" = "move workspace to output left";
        "${mod}+Control+Right" = "move workspace to output right";

        # move focused container to workspace
        "${mod}+Shift+1" = "move container to workspace 1";
        "${mod}+Shift+2" = "move container to workspace 2";
        "${mod}+Shift+3" = "move container to workspace 3";
        "${mod}+Shift+4" = "move container to workspace 4";
        "${mod}+Shift+5" = "move container to workspace 5";
        "${mod}+Shift+6" = "move container to workspace 6";

        # switch between workspaces
        "${mod}+Tab" = "workspace next";
        "${mod}+Shift+Tab" = "workspace prev";

        # reload the configuration file
        "${mod}+Shift+c" = "reload";

        # restart i3 inplace (preserves layout/session)
        "${mod}+Shift+r" = "restart";

        # exit i3 (logs out of X session)
        "${mod}+Shift+e" = "exec \"i3-nagbar -t warning -m 'You pressed the exit shortcut. Do you really want to exit i3? This will end your X session.' -b 'Yes, exit i3' 'xfce4-session-logout'\"";

        # enter resize mode
        "${mod}+r" = "mode \"resize\"";

        # enter power menu
        "${mod}+Delete" = "exec --no-startup-id \"xfce4-session-logout\"";

        # enter monitor menu
        # "${mod}+p" = "mode $monmenu";

        # noop for ctrl q to no accidentally quit stuff
        "Control+q" = "nop";

        # Clipman
        "${mod}+v" = "exec --no-startup-id \"copyq show\"";
      };
      modes = {
        resize = {
          "Left" = "resize grow width 10 px or 10 ppt";
          "Down" = "resize grow height 10 px or 10 ppt";
          "Up" = "resize shrink height 10 px or 10 ppt";
          "Right" = "resize shrink width 10 px or 10 ppt";
          "Escape" = "mode default";
          "Return" = "mode default";
        };
      };
    };

    extraConfig = ''
      default_border pixel 1
      default_floating_border pixel 1
      hide_edge_borders smart

      # Don't follow mouse
      focus_follows_mouse no

      set $powermenu "system:  [l]ock  e[x]it  [s]hut down  [r]eboot"
      set $monmenu "monitor(s):  [i]nternal  [c]lone  [e]xtended  [s]hift "


      # Albert is not working.
      # exec --no-startup-id $ {pkgs.albert}/bin/albert

      # CopyQ
      exec --no-startup-id ${pkgs.copyq}/bin/copyq

      mode $powermenu {
          bindsym l         mode "default"; exec "betterlockscreen -l dim"
          bindsym s         exec shutdown -P now
          bindsym x         exit
          bindsym r         exec shutdown -r now
          bindsym Return    mode "default"
          bindsym Escape    mode "default"
      }

      mode $monmenu {
          bindsym i         mode "default"; exec autorandr internal
          bindsym c         mode "default"; exec autorandr clone
          bindsym e         mode "default"; exec autorandr gbar
          bindsym s         mode "default"; exec /home/ahmad/.config/i3/monitor.sh
          bindsym Return    mode "default"
          bindsym Escape    mode "default"
      }

      ## Floating
      for_window [class="Telegram"] floating enable
      for_window [class="Tandem"] floating enable
      for_window [class="Mousepad"] floating enable
      for_window [class="Spotify"] floating enable
      for_window [class="tidal-hifi"] floating enable
      for_window [class="Steam"] floating enable
      for_window [class="Insomnia"] floating enable
      for_window [class="Putty"] floating enable
      for_window [class="discord"] floating enable
      for_window [title="Microsoft Teams - Preview"] floating enable

      ## Dialogs
      for_window [window_role="pop-up"] floating enable
      for_window [window_role="bubble"] floating enable
      for_window [window_role="task_dialog"] floating enable
      for_window [window_role="Preferences"] floating enable
      for_window [window_type="dialog"] floating enable
      for_window [window_type="menu"] floating enable
      for_window [class="copyq"] floating enable
      for_window [title="Chat | Microsoft Teams"] floating enable
      for_window [title="Microsoft Teams Notification"] floating enable
      for_window [class="(.*join\?action\=join.*|.*zoom.*)"] floating enable
      for_window [class="(.*join\?action\=join.*|.*zoom.*)" title="Zoom - Licensed Account"] floating disable
      for_window [class="(.*join\?action\=join.*|.*zoom.*)" title="Zoom Meeting"] floating disable
    '';
  };
}
