{
  inputs,
  outputs,
  lib,
  config,
  pkgs,
  vars,
  ...
}: let
  mod = "Mod4";
in {
  imports = [
    ./i3config.nix
  ];

  programs.i3status-rust.enable = true;
  programs.i3status-rust.bars.main = {
    theme = "gruvbox-dark";
    icons = "awesome4";
    blocks = [
      {
        block = "focused_window";
        format = " $title.str(max_w:90) |";
      }
      {
        block = "disk_space";
        path = "/";
        info_type = "available";
        alert_unit = "GB";
        interval = 20;
        format = " $icon $available.eng(w:2) ";
      }
      {
        block = "cpu";
        interval = 10;
      }
      {
        block = "temperature";
        interval = 10;
        format = " $icon $average";
        info = 70;
      }
      {
        block = "backlight";
      }
      {
        block = "music";
        format = " $icon $combo.str(max_w:21) $play $next |";
        interface_name_exclude = [".*kdeconnect.*" "mpd"];
      }
      {
        block = "sound";
        max_vol = 200;
        step_width = 2;
        # toggle_mute = "left";
        click = [
          {
            button = "left";
            action = "toggle_mute";
          }
        ];
      }
      {
        block = "battery";
        device = "DisplayDevice";
        driver = "upower";
        format = " $icon $percentage $time";
      }
      {
        block = "net";
        format = " $icon $ip";
        interval = 30;
      }
      {
        block = "time";
        interval = 5;
        format = " $timestamp.datetime(f:'%a %d/%m %R') ";
      }
    ];
  };
}
