{
  inputs,
  outputs,
  lib,
  config,
  pkgs,
  vars,
  ...
}: {
  programs.vscode = {
    enable = true;
    enableExtensionUpdateCheck = false;
    enableUpdateCheck = false;

    extensions = with pkgs.vscode-extensions;
      [
        asvetliakov.vscode-neovim

        # LSPs
        # rust-lang.rust-analyzer
        ms-vsliveshare.vsliveshare
        jnoortheen.nix-ide
        graphql.vscode-graphql
	ocamllabs.ocaml-platform

        # Syntax and formatters
        kamadorueda.alejandra
        dbaeumer.vscode-eslint
        graphql.vscode-graphql-syntax
        biomejs.biome

        # others
        github.copilot
        github.copilot-chat
        tamasfe.even-better-toml
        ms-vscode-remote.remote-ssh
        eamodio.gitlens
        mkhl.direnv
      ]
      ++ pkgs.vscode-utils.extensionsFromVscodeMarketplace [
        {
          publisher = "rust-lang";
          name = "rust-analyzer";
          version = "0.4.2038";
          sha256 = "sha256-VdGU7V1nCjh9JMIuvV6mVjtbbws3h0gdY4rZjgCne24=";
        }
        {
          publisher = "arcanis";
          name = "vscode-zipfs";
          version = "3.0.0";
          sha256 = "sha256-yNRC03kV0UvpEp1gF+NK0N3iCoqZMQ+PAqtrHLXFeXM=";
        }
        {
          publisher = "IvanDemchenko";
          name = "roc-lang-unofficial";
          version = "1.2.0";
          sha256 = "sha256-lMN6GlUM20ptg1c6fNp8jwSzlCzE1U0ugRyhRLYGPGE=";
        }
        {
          publisher = "zjhmale";
          name = "Idris";
          version = "0.9.8";
          sha256 = "sha256-t2nnLWcK1oPxSBhKKw7t39sYVDKwPx5zK87C5V8O0LU=";
        }
      ];

    mutableExtensionsDir = false;

    userSettings = {
      "extensions.experimental.affinity" = {
        "asvetliakov.vscode-neovim" = 1;
      };
      "nix.enableLanguageServer" = true;
      "nix.serverPath" = "nil";

      "git.confirmSync" = false;
      "update.showReleaseNotes" = false;
      "rust-analyzer.server.path" = "rust-analyzer";
      "editor.stickyScroll.enabled" = true;
      "editor.fontSize" = 16;
      "workbench.sideBar.location" = "right";
      "workbench.startupEditor" = "none";
      "extensions.ignoreRecommendations" = true;
      "window.menuBarVisibility" = "toggle";
      "editor.formatOnSave" = true;
      "editor.rulers" = [80 100];
      "files.insertFinalNewline" = false;
      "editor.wordWrap" = "on";
      "github.copilot.enable" = {
        "*" = true;
        "plaintext" = false;
        "markdown" = false;
        "scminput" = false;
        #  "coq" = false;
      };
    };
  };
}
