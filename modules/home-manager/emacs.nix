{
  inputs,
  outputs,
  lib,
  config,
  pkgs,
  vars,
  ...
}: {
  programs.emacs = {
    enable = true;
    extraPackages = epkgs:
      with epkgs; [
        idris-mode
	evil
	evil-leader
	direnv
      ];
    extraConfig = ''
        (use-package direnv
          :config
          (direnv-mode))
        (global-evil-leader-mode)
        (require 'evil)
        (evil-mode 1)
	(use-package idris-mode
          ensure t
	  (idris-define-evil-keys))
	(setq make-backup-files nil) ; stop creating ~ files
    '';
  };
}
