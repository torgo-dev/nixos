{
  inputs,
  outputs,
  lib,
  config,
  pkgs,
  vars,
  ...
}: {
  services.picom = {
    enable = true;
    vSync = true;

    inactiveOpacity = 1;
    activeOpacity = 1;

    shadowExclude = [
      "name = 'Notification'"
      "name = 'Albert'"
      "_GTK_FRAME_EXTENTS@:c"
      "class_g = 'i3-frame'"
      "_NET_WM_STATE@:32a *= '_NET_WM_STATE_HIDDEN'"
      "_NET_WM_STATE@:32a *= '_NET_WM_STATE_STICKY'"
      "!I3_FLOATING_WINDOW@:c"
    ];
  };
}
