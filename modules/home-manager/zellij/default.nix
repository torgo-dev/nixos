{
  inputs,
  outputs,
  lib,
  config,
  pkgs,
  vars,
  ...
}: {
  xdg.configFile."zellij/config.kdl" = {
    source = ./config.kdl;
  };

  programs.zellij = {
    enable = true;
    enableZshIntegration = false;
  };
}
