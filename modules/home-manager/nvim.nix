{
  inputs,
  outputs,
  lib,
  config,
  pkgs,
  vars,
  ...
}: {
  programs.neovim = {
    enable = true;
    #defaultEditor = true;
    vimAlias = true;
    plugins = [
      pkgs.vimPlugins.Coqtail
      pkgs.vimPlugins.idris-vim
      pkgs.vimPlugins.vim-leader-guide
    ];
    extraLuaConfig = ''
    
      vim.keymap.set("n", "<Space>", "<Nop>", { silent = true, remap = false })
      vim.g.mapleader = " "
    '';
    extraConfig = ''
      set clipboard+=unnamedplus
      set ignorecase smartcase
      " let mapleader = '\<Space>'
      nnoremap <silent> <leader> :<c-u>LeaderGuide '<Space>'<CR>
      vnoremap <silent> <leader> :<c-u>LeaderGuideVisual '<Space>'<CR>
      syntax on
      filetype on
      filetype plugin indent on

      function CoqtailHookDefineMappings()
	imap <buffer> <S-Down> <Plug>CoqNext
	imap <buffer> <S-Left> <Plug>CoqToLine
	imap <buffer> <S-Up> <Plug>CoqUndo
	nmap <buffer> <S-Down> <Plug>CoqNext
	nmap <buffer> <S-Left> <Plug>CoqToLine
	nmap <buffer> <S-Up> <Plug>CoqUndo
      endfunction
    '';
  };
}
