{
  i3wm = import ./i3wm;
  alacritty = import ./alacritty.nix;
  picom = import ./picom.nix;
  starship = import ./starship.nix;
  shell = import ./shell;
  vscode = import ./vscode;
  nvim = import ./nvim.nix;
  helix = import ./helix.nix;
  syncthing = import ./syncthing.nix;
  zellij = import ./zellij;
  wezterm = import ./wezterm;
  rofi = import ./rofi.nix;
  emacs = import ./emacs.nix;
}
