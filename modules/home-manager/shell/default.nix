{
  inputs,
  outputs,
  lib,
  config,
  pkgs,
  vars,
  ...
}: {
  programs.zoxide = {
    enable = true;

    enableZshIntegration = true;
  };

  programs.zsh = {
    enable = true;

    oh-my-zsh = {
      enable = true;
      plugins = ["git" "extract"];
    };

    initExtra = ''
      eval "$(direnv hook zsh)"

      # delete words backwards and forwards
      bindkey "^[[1;5C" forward-word
      bindkey "^[[1;5D" backward-word
      bindkey '^H' backward-kill-word
      bindkey -M emacs '^[[3;5~' kill-word

      _fix_cursor() {
        echo -ne '\e[5 q'
      }

      precmd_functions+=(_fix_cursor)
    '';

    envExtra = ''
      NODE_OPTIONS=--max_old_space_size=8192
      ERL_AFLAGS="-kernel shell_history enabled"
    '';

    shellAliases = {
      ":q" = "exit";
      gls = "git log --oneline --graph --decorate";
      glr = "git reflog";
      vim = "nvim";
      ls = "eza";
      ll = "eza -l";
      la = "eza -la";
      find = "fd";
      grep = "rg";
      cat = "bat";
      loc = "tokei";
      top = "bottom";
      gs = "git-stack";
    };
  };
}
