{
  inputs,
  outputs,
  lib,
  config,
  pkgs,
  vars,
  ...
}: {
  programs.wezterm = {
    enable = true;
    extraConfig = ''
      local wezterm = require 'wezterm'
      local config = {}

      config.color_scheme = 'GruvboxDarkHard'
      config.hide_tab_bar_if_only_one_tab = true
      config.window_decorations = 'NONE'
      config.cursor_blink_ease_in = 'Constant'
      config.cursor_blink_ease_out = 'Constant'

      local scheme = wezterm.get_builtin_color_schemes()['GruvboxDarkHard']
      scheme.ansi[1] = '#928374'

      config.color_schemes = {
          ['GruvboxDarkHard'] = scheme,
      }

      return config
    '';
  };
}
