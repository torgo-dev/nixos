{
  inputs,
  outputs,
  lib,
  config,
  pkgs,
  vars,
  ...
}: {
  services = {
    syncthing = {
      enable = true;
      #tray.enable = true;
      #dataDir = "/home/${vars.user}/Documents/sync";
    };
  };
}
