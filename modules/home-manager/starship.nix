{
  inputs,
  outputs,
  lib,
  config,
  pkgs,
  vars,
  ...
}: {
  programs.starship = {
    enable = true;

    enableZshIntegration = true;

    settings = {
      add_newline = false;
      line_break.disabled = true;

      character = {
        success_symbol = "[λ](bold green)";
        error_symbol = "[λ](bold red)";
      };
      aws = {
        format = "on [$symbol($profile )(\\($region\\) )]($style)";
      };
      #aws.disabled = true;
      nix_shell.disabled = true;
      rust.disabled = true;
      scala.disabled = true;
      elixir.disabled = true;
      package.disabled = true;
      nodejs.disabled = true;
    };
  };
}
