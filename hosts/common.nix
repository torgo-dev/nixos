{
  pkgs,
  inputs,
  outputs,
  lib,
  config,
  vars,
  ...
}: {
  # Hostnames should be set by the host config.
  # networking.hostName = "";
  networking.networkmanager.enable = true;
  networking.nameservers = ["1.1.1.1" "1.0.0.1"];

  # Set your time zone.
  time.timeZone = "Europe/Copenhagen";

  # Don't make assumptions of a graphical environment.
  # Nor to use x11 by default
  # services.xserver.enable = true;
  # Configure keymap in X11
  # services.xserver.layout = "dk";
  # services.xserver.xkbOptions = "caps:escape";

  virtualisation.docker.enable = true;
  environment = {
    sessionVariables = {
      SSH_AUTH_SOCK = "$XDG_RUNTIME_DIR/gcr/ssh";
      COSMIC_DATA_CONTROL_ENABLED = 1;
      NIXOS_OZONE_WL = "1";
    };
  };

  # Don't forget to set a password with ‘passwd’.
  users.users.${vars.user} = {
    isNormalUser = true;
    extraGroups = [
      # Enable ‘sudo’ for the user.
      "input"
      "wheel"
      "networkmanager"
      "video"
      "audio"
      "docker"
      "camera"
      "kvm"
      "nix"
      "dialout"
    ];
  };

  # Some programs need SUID wrappers, can be configured further or are
  # started in user sessions.
  # programs.mtr.enable = true;
  programs.gnupg.agent = {
    enable = true;
    enableSSHSupport = true;
  };

  programs.nix-ld.enable = true;

  # This will add each flake input as a registry
  # To make nix3 commands consistent with your flake
  nix.registry = (lib.mapAttrs (_: flake: {inherit flake;})) ((lib.filterAttrs (_: lib.isType "flake")) inputs);

  # This will additionally add your inputs to the system's legacy channels
  # Making legacy nix commands consistent as well, awesome!
  nix.nixPath = ["/etc/nix/path"];
  environment.etc =
    lib.mapAttrs'
    (name: value: {
      name = "nix/path/${name}";
      value.source = value.flake;
    })
    config.nix.registry;

  nix.settings = {
    # Enable flakes and new 'nix' command
    experimental-features = "nix-command flakes";
    # Deduplicate and optimize nix store
    auto-optimise-store = true;
    trusted-users = ["root" "ahmad"];
  };

  nixpkgs = {
    # You can add overlays here
    overlays = [
      # Add overlays your own flake exports (from overlays and pkgs dir):
      outputs.overlays.additions
      outputs.overlays.modifications
      outputs.overlays.unstable-packages

      # You can also add overlays exported from other flakes:
      # neovim-nightly-overlay.overlays.default

      # Or define it inline, for example:
      # (final: prev: {
      #   hi = final.hello.overrideAttrs (oldAttrs: {
      #     patches = [ ./change-hello-to-hi.patch ];
      #   });
      # })
    ];
    # Configure your nixpkgs instance
    config = {
      # Disable if you don't want unfree packages
      allowUnfree = true;
      # allowBroken = true;
    };
  };

  # Quirk with eduroam
  systemd.services.wpa_supplicant.environment.OPENSSL_CONF = pkgs.writeText "openssl.cnf" ''
    openssl_conf = openssl_init
    [openssl_init]
    ssl_conf = ssl_sect
    [ssl_sect]
    system_default = system_default_sect
    [system_default_sect]
    Options = UnsafeLegacyRenegotiation
    [system_default_sect]
    CipherString = Default:@SECLEVEL=0
  '';

  system.stateVersion = "23.05";
}
