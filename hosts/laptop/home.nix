# This is your home-manager configuration file
# Use this to configure your home environment (it replaces ~/.config/nixpkgs/home.nix)
{
  inputs,
  outputs,
  lib,
  config,
  pkgs,
  vars,
  ...
}: {
  # You can import other home-manager modules here
  imports = [
    # To use modules eexported from flakes:
    # inputs.nix-colors.homeManagerModules.default
    outputs.homeManagerModules.alacritty
    outputs.homeManagerModules.i3wm
    outputs.homeManagerModules.wezterm
    outputs.homeManagerModules.shell
    outputs.homeManagerModules.starship
    outputs.homeManagerModules.vscode
    outputs.homeManagerModules.syncthing
    outputs.homeManagerModules.nvim
    outputs.homeManagerModules.zellij
    outputs.homeManagerModules.helix
    outputs.homeManagerModules.rofi
    outputs.homeManagerModules.emacs
  ];

  nixpkgs = {
    # You can add overlays here
    overlays = [
      # Add overlays your own flake exports (from overlays and pkgs dir):
      outputs.overlays.additions
      outputs.overlays.modifications
      outputs.overlays.unstable-packages

      # You can also add overlays exported from other flakes:
      # neovim-nightly-overlay.overlays.default

      # Or define it inline, for example:
      # (final: prev: {
      #   hi = final.hello.overrideAttrs (oldAttrs: {
      #     patches = [ ./change-hello-to-hi.patch ];
      #   });
      # })
    ];
    # Configure your nixpkgs instance
    config = {
      # Disable if you don't want unfree packages
      allowUnfree = true;
      # Workaround for https://github.com/nix-community/home-manager/issues/2942
      allowUnfreePredicate = _: true;

      permittedInsecurePackages = [
        "electron-25.9.0"
      ];
    };
  };

  home = {
    username = vars.user;
    homeDirectory = "/home/${vars.user}";
  };

  home.packages = with pkgs; [
    zellij
    eza
    fd
    bat
    tokei
    bottom
    htop
    copyq
    obsidian
    beeper
    alejandra
    nil
    discord
    xclip
    openfortivpn
    telegram-desktop
    evince
    xfce.mousepad
    parsec-bin
    hyperfine
    libsForQt5.ark
    barrier
    gnumake
    gcc13
    qalculate-qt
    obs-studio
    protonvpn-gui
    arandr
    chromium
    loupe
    mpv
    vlc
    stremio
    teams-for-linux
    unstable.zed-editor
    lutris-unwrapped
    jujutsu
    steam
    (pkgs.zoom-us.overrideAttrs {
      version = "6.2.11.5069";
      src = pkgs.fetchurl {
        url = "https://zoom.us/client/6.2.11.5069/zoom_x86_64.pkg.tar.xz";
        hash = "sha256-k8T/lmfgAFxW1nwEyh61lagrlHP5geT2tA7e5j61+qw=";
      };
    })
  ];

  # services.lorri.enable = true;

  # Enable home-manager and git
  programs.home-manager.enable = true;
  programs.git = {
    enable = true;
    userName = "Ahmad Sattar";
    userEmail = "thehabbos007@gmail.com";
  };

  programs.direnv = {
    enable = true;
    enableZshIntegration = true;
    nix-direnv.enable = true;
  };

  systemd.user.targets.tray = {
    Unit = {
      Description = "Home Manager System Tray";
      Requires = ["graphical-session-pre.target"];
    };
  };

  services.redshift = {
    enable = true;
    tray = true;
    latitude = "55.676098";
    longitude = "12.568337";
  };

  systemd.user.startServices = "sd-switch";

  # https://nixos.wiki/wiki/FAQ/When_do_I_update_stateVersion
  home.stateVersion = "23.11";
}
