# System configuration for torgo
{
  inputs,
  outputs,
  pkgs,
  vars,
  ...
}: {
  imports = [
    inputs.home-manager.nixosModules.home-manager
    inputs.hardware.nixosModules.common-cpu-amd
    inputs.hardware.nixosModules.common-gpu-amd
    inputs.hardware.nixosModules.common-pc-laptop
    inputs.hardware.nixosModules.common-pc-laptop-ssd
    inputs.nixos-cosmic.nixosModules.default
    ./hardware-configuration.nix
  ];

  nix.settings = {
    substituters = ["https://cosmic.cachix.org/"];
    trusted-public-keys = ["cosmic.cachix.org-1:Dya9IyXD4xdBehWjrkPv6rtxpmMdRel02smYzA85dPE="];
  };

  programs.zsh.enable = true;
  programs.kdeconnect.enable = true;
  users.users.${vars.user}.shell = pkgs.zsh;

  i18n.defaultLocale = "en_DK.UTF-8";
  i18n.extraLocaleSettings = {
    LC_ADDRESS = "da_DK.UTF-8";
    LC_IDENTIFICATION = "da_DK.UTF-8";
    LC_MEASUREMENT = "da_DK.UTF-8";
    LC_MONETARY = "da_DK.UTF-8";
    LC_NAME = "da_DK.UTF-8";
    LC_NUMERIC = "da_DK.UTF-8";
    LC_PAPER = "da_DK.UTF-8";
    LC_TELEPHONE = "da_DK.UTF-8";
    LC_TIME = "da_DK.UTF-8";
  };

  services.xserver = {
    xkb = {
      layout = "dk";
      variant = "";
    };
  };

  services.libinput = {
    enable = true;
    touchpad = {
      naturalScrolling = true;
      tapping = true;
      disableWhileTyping = true;
    };
  };

  console.keyMap = "dk-latin1";

  home-manager = {
    extraSpecialArgs = {inherit inputs outputs vars;};
    users = {
      # Import your home-manager configuration
      ${vars.user} = import ./home.nix;
    };
  };

  networking = {
    hostName = "torgo";
  };

  boot.kernelPackages = pkgs.linuxPackages_latest;

  services.displayManager = {
    cosmic-greeter.enable = true;
    # defaultSession = "xfce+i3";
    autoLogin = {
      enable = true;
      user = vars.user;
    };
  };

  services.desktopManager.cosmic.enable = true;
  # services.displayManager.cosmic-greeter.enable = true;

  # services.xserver = {
  #   enable = true;
  #   videoDrivers = ["amdgpu"];
  #   deviceSection = ''
  #     Option "DRI" "2"
  #     Option "TearFree" "true"
  #   '';

  #   desktopManager = {
  #     xterm.enable = false;
  #     xfce = {
  #       enable = true;
  #       noDesktop = true;
  #       enableXfwm = false;
  #     };
  #   };

  #   windowManager.i3.enable = true;

  #   displayManager = {
  #     lightdm = {
  #       enable = true;
  #       greeter.enable = false;
  #     };
  #   };
  # };

  services.gnome.gnome-keyring.enable = true;
  security.pam.services.gdm.enableGnomeKeyring = true;

  # Use the systemd-boot EFI boot loader.
  boot.loader.systemd-boot.enable = true;
  boot.loader.efi.canTouchEfiVariables = true;
  boot.loader.systemd-boot.configurationLimit = 5;

  services.dbus.enable = true;

  services.tlp = {
    settings = {
      CPU_BOOST_ON_AC = 1;
      CPU_BOOST_ON_BAT = 0;
      CPU_SCALING_GOVERNOR_ON_AC = "performance";
      CPU_SCALING_GOVERNOR_ON_BAT = "powersave";
    };
  };

  # Enable CUPS to print documents.
  services.printing = {
    enable = true;
    # drivers = [pkgs.hplipWithPlugin];
  };

  # Enable sound with pipewire.
  services.pulseaudio.enable = false;
  security.rtkit.enable = true;
  services.pipewire = {
    enable = true;
    alsa.enable = true;
    alsa.support32Bit = true;
    pulse.enable = true;
    jack.enable = true;
  };

  # Bleutooth
  hardware.bluetooth.enable = true;
  hardware.bluetooth.powerOnBoot = true;
  services.blueman.enable = true;

  virtualisation.docker.enable = true;

  environment.systemPackages = with pkgs; [
    zip
    unzip
    vim
    nano
    git
    wget
    ripgrep
    killall
    pango
    firefox
    kitty
    dbus
    linuxKernel.packages.linux_zen.perf
    tailscale
    pavucontrol
    wl-clipboard-rs
    gcr_4
    #orca-slicer
    cosmic-ext-applet-clipboard-manager
  ];

  services.tailscale.enable = true;

  environment = {
    variables = {
      TERMINAL = "wezterm";
    };
  };

  fonts = {
    fontconfig.enable = true;
    packages = with pkgs; [
      corefonts
      vistafonts
      fira
      # Emoji
      noto-fonts
      # font-awesome
      font-awesome_4
      # iosevka-bin
      nerd-fonts.zed-mono
      nerd-fonts.jetbrains-mono
      nerd-fonts.iosevka
      nerd-fonts.iosevka-term
    ];
  };

  # List services that you want to enable:

  console.earlySetup = true;
}
