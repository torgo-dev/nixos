# System configuration for torgo
{
  inputs,
  outputs,
  pkgs,
  vars,
  ...
}: {
  imports = [
    inputs.home-manager.nixosModules.home-manager
    #inputs.hardware.nixosModules.dell-xps-15-9520
    #inputs.hardware.nixosModules.dell-xps-15-9520-nvidia
    inputs.nixos-cosmic.nixosModules.default
    ./hardware-configuration.nix
  ];

  nix.settings = {
    substituters = ["https://cosmic.cachix.org/"];
    trusted-public-keys = ["cosmic.cachix.org-1:Dya9IyXD4xdBehWjrkPv6rtxpmMdRel02smYzA85dPE="];
  };

  programs.zsh.enable = true;
  users.users.${vars.user}.shell = pkgs.zsh;

  i18n.defaultLocale = "en_DK.UTF-8";
  i18n.extraLocaleSettings = {
    LC_ADDRESS = "da_DK.UTF-8";
    LC_IDENTIFICATION = "da_DK.UTF-8";
    LC_MEASUREMENT = "da_DK.UTF-8";
    LC_MONETARY = "da_DK.UTF-8";
    LC_NAME = "da_DK.UTF-8";
    LC_NUMERIC = "da_DK.UTF-8";
    LC_PAPER = "da_DK.UTF-8";
    LC_TELEPHONE = "da_DK.UTF-8";
    LC_TIME = "da_DK.UTF-8";
  };

  services.hardware.bolt.enable = true;
  # services.fwupd.enable = true;
  services.xserver = {
    xkb = {
      layout = "dk";
      variant = "";
    };
    autoRepeatDelay = 240;
    autoRepeatInterval = 40;
  };

  services.xserver.config = ''
    Section "InputClass"
        Identifier "External Keyboard"
        MatchProduct "Keycapsss Lily58L"
        Option "XkbLayout" "us"
        Option "XkbVariant" "altgr-intl"
        Option "XkbOptions" "nodeadkeys"
    EndSection
  '';

  services.libinput = {
    enable = true;
    touchpad = {
      naturalScrolling = true;
      tapping = true;
      disableWhileTyping = true;
    };
  };

  console.keyMap = "dk-latin1";

  home-manager = {
    extraSpecialArgs = {inherit inputs outputs vars;};
    users = {
      # Import your home-manager configuration
      ${vars.user} = import ./home.nix;
    };
  };

  networking = {
    hostName = "ahatbird";
  };

  boot.kernelParams = ["usbcore.autosuspend=-1"];
  boot.binfmt.emulatedSystems = ["aarch64-linux"];

  boot.kernelPackages = pkgs.linuxPackages_latest;
  hardware.nvidia = {
    open = true;

    powerManagement = {
      enable = true;
      finegrained = true;
    };

    prime = {
      offload.enable = true;
      nvidiaBusId = "0@01:00:0"; # Found with lspci | grep VGA
      intelBusId = "1@00:02:0"; # Found with lspci | grep VGA
    };
  };

  services.power-profiles-daemon.enable = true;
  services.thermald.enable = true;
  services.tlp = {
    settings = {
      CPU_BOOST_ON_AC = 1;
      CPU_BOOST_ON_BAT = 0;
      CPU_SCALING_GOVERNOR_ON_AC = "performance";
      CPU_SCALING_GOVERNOR_ON_BAT = "powersave";
    };
  };

  services.displayManager = {
    cosmic-greeter.enable = true;
    # defaultSession = "xfce+i3";
    autoLogin = {
      enable = true;
      user = vars.user;
    };
  };

  services.desktopManager.cosmic.enable = true;

  services.gnome.gnome-keyring.enable = true;
  security.pam.services.gdm.enableGnomeKeyring = true;

  # Use the systemd-boot EFI boot loader.
  boot.loader.systemd-boot.enable = true;
  boot.loader.efi.canTouchEfiVariables = true;
  boot.loader.systemd-boot.configurationLimit = 5;

  services.dbus.enable = true;

  # Enable CUPS to print documents.
  services.printing.enable = true;

  # Bleutooth
  hardware.bluetooth.enable = true;
  hardware.bluetooth.powerOnBoot = true;
  services.blueman.enable = true;

  # Enable pulse audio
  services.pulseaudio.enable = false;

  # Enable sound with pipewire.
  security.rtkit.enable = true;
  services.pipewire = {
    enable = true;
    alsa.enable = true;
    alsa.support32Bit = true;
    pulse.enable = true;
    jack.enable = true;
    wireplumber.enable = true;
  };

  virtualisation.docker.enable = true;

  environment.systemPackages = with pkgs; [
    zip
    unzip
    vim
    nano
    git
    wget
    ripgrep
    killall
    pango
    firefox
    alacritty
    kitty
    lsof
    # fprintd
    imagemagick
    chromium
    time
    file
    libsForQt5.ark
    binutils
    wl-clipboard-rs
    gcr_4
    cosmic-ext-applet-clipboard-manager
  ];

  environment = {
    variables = {
      TERMINAL = "alacritty";
    };
  };

  fonts.packages = with pkgs; [
    corefonts
    # font-awesome
    font-awesome_4
    # iosevka-bin
    nerd-fonts.zed-mono
    nerd-fonts.jetbrains-mono
    nerd-fonts.iosevka
    nerd-fonts.iosevka-term
  ];

  # List services that you want to enable:

  console.earlySetup = true;

  # Fingerprint reader
  # services.fprintd.enable = true;
  # services.fprintd.tod.enable = true;
  # services.fprintd.tod.driver = pkgs.libfprint-2-tod1-goodix;
}
