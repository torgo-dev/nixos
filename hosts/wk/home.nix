# This is your home-manager configuration file
# Use this to configure your home environment (it replaces ~/.config/nixpkgs/home.nix)
{
  inputs,
  outputs,
  lib,
  config,
  pkgs,
  vars,
  ...
}: {
  # You can import other home-manager modules here
  imports = [
    # To use modules eexported from flakes:
    # inputs.nix-colors.homeManagerModules.default

    outputs.homeManagerModules.i3wm
    outputs.homeManagerModules.picom
    outputs.homeManagerModules.alacritty
    outputs.homeManagerModules.wezterm
    outputs.homeManagerModules.shell
    outputs.homeManagerModules.starship
    outputs.homeManagerModules.vscode
    outputs.homeManagerModules.nvim
    outputs.homeManagerModules.helix
    outputs.homeManagerModules.rofi
    outputs.homeManagerModules.emacs
  ];

  nixpkgs = {
    # You can add overlays here
    overlays = [
      # Add overlays your own flake exports (from overlays and pkgs dir):
      outputs.overlays.additions
      outputs.overlays.modifications
      outputs.overlays.unstable-packages

      # You can also add overlays exported from other flakes:
      # neovim-nightly-overlay.overlays.default

      # Or define it inline, for example:
      # (final: prev: {
      #   hi = final.hello.overrideAttrs (oldAttrs: {
      #     patches = [ ./change-hello-to-hi.patch ];
      #   });
      # })
    ];
    # Configure your nixpkgs instance
    config = {
      # Disable if you don't want unfree packages
      allowUnfree = true;
      # Workaround for https://github.com/nix-community/home-manager/issues/2942
      allowUnfreePredicate = _: true;
    };
  };

  home = {
    username = vars.user;
    homeDirectory = "/home/${vars.user}";
  };

  home.packages = with pkgs; [
    zellij
    eza
    fd
    bat
    tokei
    bottom
    htop
    #copyq
    slack
    nitrogen
    bat
    xclip
    pavucontrol
    nil
    thunderbird
    evince
    tidal-hifi
    libsForQt5.kate
    ast-grep
    bruno
    discord
    jq
    jujutsu
    zed-editor
    git-stack
    hoppscotch
    teams-for-linux
    onlyoffice-desktopeditors
  ];

  programs.granted = {
    enable = true;
    enableZshIntegration = true;
  };
  programs.ssh = {
    enable = true;
    addKeysToAgent = "yes";
  };

  services.ssh-agent = {
    enable = true;
  };

  programs.atuin = {
    enable = true;
    enableZshIntegration = true;
    flags = ["--disable-up-arrow"];
  };

  programs.direnv = {
    enable = true;
    enableZshIntegration = true;
    nix-direnv.enable = true;
  };

  # Enable home-manager and git
  programs.home-manager.enable = true;
  programs.git = {
    enable = true;
    userName = "Ahmad Sattar";
    userEmail = "thehabbos007@gmail.com";
  };

  systemd.user.startServices = "sd-switch";

  # https://nixos.wiki/wiki/FAQ/When_do_I_update_stateVersion
  home.stateVersion = "23.11";
}
