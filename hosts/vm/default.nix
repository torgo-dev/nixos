# System configuration for torgo
{
  inputs,
  outputs,
  pkgs,
  vars,
  ...
}: {
  imports = [
    inputs.home-manager.nixosModules.home-manager

    ./hardware-configuration.nix
  ];

  programs.zsh.enable = true;
  users.users.${vars.user}.shell = pkgs.zsh;

  home-manager = {
    extraSpecialArgs = {inherit inputs outputs vars;};
    users = {
      # Import your home-manager configuration
      ${vars.user} = import ./vm-home.nix;
    };
  };

  networking = {
    hostName = "vm";
  };

  boot.kernelPackages = pkgs.linuxPackages_latest;

  services.xserver = {
    enable = true;
    desktopManager = {
      xterm.enable = false;
      xfce = {
        enable = true;
        noDesktop = true;
        enableXfwm = false;
      };
    };

    windowManager.i3.enable = true;

    displayManager = {
      defaultSession = "xfce+i3";
      lightdm = {
        enable = true;
        greeter.enable = false;
      };

      autoLogin = {
        enable = true;
        user = vars.user;
      };
    };
  };

  services.picom = {
    enable = true;
  };

  # Use the systemd-boot EFI boot loader.
  boot.loader.systemd-boot.enable = true;
  boot.loader.efi.canTouchEfiVariables = true;
  boot.loader.systemd-boot.configurationLimit = 5;

  services.dbus.enable = true;

  # Enable CUPS to print documents.
  services.printing.enable = true;

  # Enable sound with pipewire.
  hardware.pulseaudio.enable = false;
  security.rtkit.enable = true;
  services.pipewire = {
    enable = true;
    alsa.enable = true;
    alsa.support32Bit = true;
    pulse.enable = true;
    jack.enable = true;
  };

  virtualisation.docker.enable = true;

  environment.systemPackages = with pkgs; [
    zip
    unzip
    vim
    neovim
    nano
    git
    wget
    ripgrep
    killall
    pango
    firefox
    alacritty
    kitty
  ];

  environment = {
    variables = {
      TERMINAL = "alacritty";
    };
  };

  fonts.packages = with pkgs; [
    corefonts
    # font-awesome
    font-awesome_5
    # iosevka-bin
    (nerdfonts.override {fonts = ["Iosevka" "IosevkaTerm" "JetBrainsMono"];})
  ];

  # List services that you want to enable:

  console.earlySetup = true;
}
